﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityMvcApp.BLL;
using UniversityMvcApp.DAL.Gateway;
using UniversityMvcApp.Models;

namespace UniversityMvcApp.Controllers
{
    public class EnrollCoursesController : Controller
    {
        private UniversityDbContext db = new UniversityDbContext();
        EnrollCourseManager enrollCourseManager = new EnrollCourseManager();
        // GET: EnrollCourses
        public ActionResult Add()
        {
            ViewBag.StudentId = db.Students.ToList();
            return View();
        }
        [HttpPost]
        public ActionResult Add(EnrollCourses enrollCourse)
        {
            ViewBag.StudentId = db.Students.ToList();
            var courses = db.EnrollCourses.ToList();
            var course = courses.Where(a => (a.CourseId == enrollCourse.CourseId) && (a.StudentId==enrollCourse.StudentId) ).ToList();

            if (course.Count == 0)
            {
                if (ModelState.IsValid)
                {
                    db.EnrollCourses.Add(enrollCourse);
                    db.SaveChanges();
                    ViewBag.saveMessage = "Student Course Enrolled Successfull!";

                }

                return View();
            }
            else
            {
                ViewBag.EnrollExist = "Student has already Enrolled For thid Course";
            }

            return View();
        }

        public JsonResult GetCourseByStudentId(int studentId)
        {
            //List<Course> course = db.Courses.ToList();


            //var students = db.Students.ToList();
            //var student = students.Find(a => a.Id == studentId);
            //List<Course> courses = db.Courses.ToList();
            Student student = enrollCourseManager.SearchStudentById(studentId);
            List<Course> course = enrollCourseManager.GetCourseByDepartment(student);
            //return Json(course);
            return Json(course, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStudentByStudentId(int studentId)
        {
            //List<Course> course = db.Courses.ToList();


            //var students = db.Students.ToList();
            //var student = students.Find(a => a.Id == studentId);
            //List<Course> courses = db.Courses.ToList();
            Student student = enrollCourseManager.SearchStudentById(studentId);
            string Name = student.Name;
            //return Json(course);
            return Json(Name, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDepartmentNameByStudentId(int studentId)
        {
            Student student = enrollCourseManager.SearchStudentById(studentId);
            Department Department = enrollCourseManager.SearchDepartmentStudentById(student.DepartmentId);
            string Name = Department.Name;
            //return Json(course);
            return Json(Name, JsonRequestBehavior.AllowGet);
        }

    }
}